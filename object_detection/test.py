import argparse
import numpy as np
import torch
import torch.utils.data
from PIL import Image
import pandas as pd
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from engine import train_one_epoch, evaluate, test
import utils
import transforms as T
import torchvision.transforms as transforms
import torchvision
from evaluate import evaluate_map
import tensorflow as tf
import datetime

def get_model(num_classes):
    # load an object detection model pre-trained on COCO
    model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=False)
    # get the number of input features for the classifier
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    # replace the pre-trained head with a new on
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features,num_classes)
    return model

def main(args = None):
    parser = argparse.ArgumentParser(description='Simple training script for training a RetinaNet network.')
    parser.add_argument('--data_augment', action='store_true', help='set this flag to enable data augmentation')
    parser.add_argument('--data_balancing', action='store_true', help='set this flag to enable class balancing')
    parser.add_argument('--epochs', help='Number of epochs', type=int, default=50)
    parser.add_argument('--model', help='Model to evaluate')
    args = parser.parse_args(args)
    
    classes = ('Aerosol','Bout./flac. en plastique < 8L', "Bout./flac. en plastique > 8L",
           "Emballage métallique", "Huile de friture", "Carton à boisson", "Papier-carton",
           "Verre incolore", "Verre coloré", "FR", "PDP", "DSM")
    # classes = ('Bout./flac. en plastique', "Emballage metallique", "Huile de friture", "Papier-carton","Verre","DSM")
    annotations = "/home/orumfels/datasets/triscandata/annotations/instances_"
    folder = "/home/orumfels/datasets/triscandata/"
    # annotations = "/home/orumfels/datasets/small_triscan/annotations/instances_"
    # folder = "/home/orumfels/datasets/small_triscan/"
    # annotations = "/home/orumfels/datasets/triscan_merged/annotations/instances_"
    # folder = "/home/orumfels/datasets/triscan_merged/"
    model_path = args.model

    test_dataset = CustomDataset(folder+"/val/", classes, annotations+"val.json", False)

    # define training and validation data loaders
    data_loader_test = torch.utils.data.DataLoader(test_dataset, batch_size=1, shuffle=False,
                                                   num_workers=4, collate_fn=utils.collate_fn)
    print(" {} are training and {} testing".format(train_dataset.__len__(), test_dataset.__len__() ))
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    num_classes = len(classes) #+1
    # get the model using our helper function
    model = get_model(num_classes)
    if torch.cuda.is_available() :
        model.load_state_dict(torch.load(model_path))
    else :
        model.load_state_dict(torch.load(model_path, map_location=torch.device('cpu')))
    model.eval()
    print("Beginning testing")
    test_loss = test(model, data_loader_test, device, epoch,print_freq=1000)
    print("TEST LOSS :" , test_loss)
    average_precisions, precisions, recalls, IOU, top_1_accuracy = evaluate_map(test_dataset, model, data_loader_test, device, score_threshold=0.4)
    print("AP =", average_precisions)
    print("P and R :", precisions, recalls)
    print("IOU :", IOU)
    print("top1 acc :", top_1_acc)
    
        

if __name__ == '__main__':
    main()
