import os
import torch
from PIL import Image
from torch.utils.data import Dataset
import json
import cv2
from progressbar import ProgressBar
from torchvision.ops.boxes import box_convert
import torchvision.transforms as transforms
import skimage.io
import numpy as np
import random

def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        if filename.endswith('.jpg'):
            images.append(filename)
    return images

def create_list(dataset):
    #open json file for labels
    f = open (dataset.json_file, "r")
    data = json.loads(f.read())
    f.close()
    if os.path.isfile(dataset.folder):
        filenames = [dataset.folder.split('/')[-1]]
    else :
        # Get the filenames of all training images
        filenames = load_images_from_folder(dataset.folder)
    labels = []
    bboxes = []
    images = []
    for fn in filenames:
        found_index = None
        for j, x in enumerate(data['images']):
            if x['filename'] == fn :
                found_index = j
                break
        #i is the index of found image
        if found_index is not None : #make sure annotation exists
            annot = data['annotations'][found_index]
            if dataset.background :
                labels.append(annot['category_id']  )
            else :
                labels.append(annot['category_id'] -1)
            bboxes.append(annot['bbox'])
            if os.path.isfile(dataset.folder):
                images.append(dataset.folder)
            else :
                images.append(dataset.folder + fn)
    return images, labels, bboxes

class CustomDataset(Dataset):
    """The training table dataset.
    """
    def __init__(self, folder, classes, json_file, data_augment, background):
        # initialize all variables
        self.json_file = json_file
        self.folder = folder
        self.classes = classes
        self.background = background
        # get annotations of images in folder
        filenames, labels, bboxes = create_list(self)
        self.filenames =  filenames
        self.labels = labels
        self.bboxes = bboxes
        self.len = len(self.filenames) # Size of data
        self.augment = data_augment
        # set self.transformation to None if work without a 
        self.transformation = transforms.Compose([transforms.ToPILImage(), transforms.Resize((224, 244)), transforms.ToTensor()])

    def __getitem__(self, index):
        # source : https://towardsdatascience.com/building-your-own-object-detector-pytorch-vs-tensorflow-and-how-to-even-get-started-1d314691d4ae
        img = skimage.io.imread(self.filenames[index])
        img = img.astype(np.float32)/255.0
        annot = self.bboxes[index]
        height, width, z = img.shape
        tmp_box = np.zeros(shape=(1,4))
        # copy all annotations in tmp_box
        for i, elem in enumerate(annot):
            tmp_box[0, i] = annot[i]

        if self.augment :
            sample = transform_data(img, tmp_box, self.transformation)
            tensor_image = sample['img']
            boxes = sample['annot']
        else :
            tensor_image = torch.tensor(img.copy())
            if self.transformation is not None :
                tensor_image = self.transformation(tensor_image)
            boxes = tmp_box
        
        # transform from [x, y, w, h] to [x1, y1, x2, y2]
        boxes[:, 2] = boxes[:, 0] + boxes[:, 2]
        boxes[:, 3] = boxes[:, 1] + boxes[:, 3]
        
        # resize boxes given the new size 224
        if self.transformation is not None :
            boxes[0,0] = boxes[0,0] * 224/width
            boxes[0,1] = boxes[0,1] * 224/height
            boxes[0,2] = boxes[0,2] * 224/width
            boxes[0,3] = boxes[0,3] * 224/height
        # check if no problem with resizing
        if boxes[0, 0]>= boxes[0, 2]:
            boxes[0, 2] = boxes[0, 2] + 1
        if boxes[0, 1]>= boxes[0, 3]:
            boxes[0, 3] = boxes[0, 3] + 1

        target = {}
        target["boxes"] = torch.tensor(boxes)
        target["labels"] = torch.tensor([self.labels[index]])
        target["image_id"] = torch.tensor([index])
        target["area"] = torch.tensor([(boxes[0,2]-boxes[0,0]) *(boxes[0,3]-boxes[0,1])])
        target["iscrowd"] = torch.zeros((1,), dtype = torch.int64)
        return tensor_image, target

    def __len__(self):
        return self.len

    def get_filename(self, index):
        return self.filenames[index]
    
    def get_classes(self):
        return self.classes
    def num_classes(self):
        if self.background :
            return len(self.classes)+1
        else :
            return len(self.classes)
    def label_to_name(self, i):
        if self.background :
            return self.classes[i -1]
        else :
            return self.classes[i]

def min_max(x, y, limx, limy):
    x = int(min(max(x, 0), limx))
    y = int(min(max(y, 0), limy))
    return x, y

def crop_center(img,cropx,cropy, annot, centrex, centrey):
    x = (centrex-cropx)/2
    y = (centrey-cropy)/2
    startx, starty = min_max(x, y, img.shape[0], img.shape[1])
    endx, endy = min_max(startx + cropx, starty+cropy, img.shape[0], img.shape[1])
    # pixels crop on the right
    right = img.shape[0] - (endx - startx)
    # pixels crop on top
    top = img.shape[1] - (endy - starty)
    annot2 = annot.copy()
    annot2[0,0], annot2[0,1] = min_max(annot[0,0] - startx, annot[0,1] - starty, endx-startx, endy-starty)
    annot2[0,2],annot2[0,3] = min_max(annot[0,2] - startx, annot[0,3] - starty, endx-startx, endy-starty)
    return img[starty:endy,startx:endx, :], annot2
    
def transform_data(image, init_annot, transform = None):
    """
    Input : 
    annot[0, :4] = bounding box [x1, y1, x2, y2]
    annot[0, 4] = label
    """
    transformation = None
    height = image.shape[0]
    width = image.shape[1]
    annot = init_annot.copy()
    if random.choice([True, False]):
        # do a horizontal flip
        image = np.fliplr(image)
        # new bounding box
        x1 = width - annot[0, 2]
        x2 = width - annot[0, 0]
        annot[0, 0] = x1
        annot[0, 2] = x2
    if random.choice([True, False]):
        # do a vertical flip
        image = np.flipud(image)
        # new bounding box
        y1 = height - annot[0, 1]
        y2 = height - annot[0, 3]
        annot[0, 1] = y1
        annot[0, 3] = y2
    if random.choice([True, False]):
        # do a crop
        centers_list = [[annot[0, 0], annot[0,1]], [annot[0, 0], annot[0,3]],
                        [annot[0, 2], annot[0,1]], [annot[0, 2], annot[0,3]],
                        [(annot[0, 0]+annot[0, 2])//2, (annot[0, 1]+annot[0, 3])//2]]
        center_of_crop = random.choice(centers_list)
        image, annot = crop_center(image, int(2*width/3), int(2*height/3), annot, center_of_crop[0], center_of_crop[1])
    sample = {'img': image, 'annot': annot}
    if transform is not None :
        tensor_image = torch.tensor(image.copy())
        tensor_image = transform(tensor_image)
        sample = {'img': tensor_image, 'annot': annot}
    return sample
    
#show results
def show_classification_results(dataset, model, result_folder):
    classes = dataset.get_classes()
    #switch to evaluation mode
    model.eval()
    # for all images in the test dataset
    for i in range(0, dataset.__len__()):
        filename = dataset.get_filename(i)
        x, y = dataset.__getitem__(i)
        shape = x.shape
        # create tensor with image
        tmp = torch.zeros(1, shape[0], shape[1], shape[2])
        tmp [0] = x
        # get predicted results
        output = model(tmp)
        output = torch.argmax(output).item()
        # open image and put label on it
        image = cv2.imread(dataset.folder+filename)
        text = "predicted label = " + classes[output]
        image = cv2.putText(image, text, (10, 50), cv2.FONT_HERSHEY_SIMPLEX ,  
                   1, (255, 0, 0), 2, cv2.LINE_AA)
        text = " real label = " + classes[y]
        image = cv2.putText(image, text, (10, 100), cv2.FONT_HERSHEY_SIMPLEX ,  
                   0.8, (255, 0, 0), 2, cv2.LINE_AA) # font scale lowered
        cv2.imwrite(result_folder+filename, image)
