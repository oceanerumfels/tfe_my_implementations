import argparse
import numpy as np
import torch
import torch.utils.data
from PIL import Image
import pandas as pd
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection.rpn import AnchorGenerator
from torchvision.models.detection.rpn import RPNHead
from engine import train_one_epoch, evaluate, test
import utils
import transforms as T
import torchvision.transforms as transforms
import torchvision
from evaluate import evaluate_map
import tensorflow as tf
import datetime

from my_utils import CustomDataset

def get_model(num_classes, pretrained_coco, pretrained_backbone, custom_anchors):
    # load an object detection model pre-trained on COCO
    if pretrained_backbone and pretrained_coco:
        model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True, pretrained_backbone = True)
    elif pretrained_backbone :
        model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained_backbone = True)
    else :
        model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True)
    if custom_anchors :
        # create an anchor_generator for the FPN
        # which by default has 5 outputs
        # previously 8, 16, 32, 64, 128, 224 and 0.25 0.5 1.0 2.0
        anchor_generator = AnchorGenerator(
        sizes=tuple([(64, 100, 128, 160, 190, 224) for _ in range(5)]),
        aspect_ratios=tuple([(0.25, 0.5, 0.66, 1.0, 1.5, 2.0) for _ in range(5)]))
        model.rpn.anchor_generator = anchor_generator
        # 256 because that's the number of features that FPN returns
        model.rpn.head = RPNHead(256, anchor_generator.num_anchors_per_location()[0])
    # get the number of input features for the classifier
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    # replace the pre-trained head with a new on
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features,num_classes)
    return model

def main(args = None):
    parser = argparse.ArgumentParser(description='Simple training script for training a RetinaNet network.')
    parser.add_argument('--data_augment', action='store_true', help='set this flag to enable data augmentation')
    parser.add_argument('--data_balancing', action='store_true', help='set this flag to enable class balancing')
    parser.add_argument('--background', action='store_true', help='set this flag to set class 0 to background')
    parser.add_argument('--epochs', help='Number of epochs', type=int, default=50)
    parser.add_argument('--custom_anchors', action='store_true', help='set this flag to use custom anchors')
    parser.add_argument('--pretrain_back', action='store_true', help='set this flag to pretrain backbone')
    parser.add_argument('--pretrain_coco', action='store_true', help='set this flag to pretrain all the network')
    parser.add_argument('--score_thres', type=float, default=0.05, help='Threshold for the score')
    parser.add_argument('--merge', action='store_true', help='set this flag to use merged classes')
    
    args = parser.parse_args(args)
    
    if args.merge :
        classes = ('Bout./flac. en plastique', "Emballage metallique", "Huile de friture", "Papier-carton","Verre","DSM")
        annotations = "/home/orumfels/datasets/triscan_merged/annotations/instances_"
        folder = "/home/orumfels/datasets/triscan_merged/"
    else :
        classes = ('Aerosol','Bout./flac. en plastique < 8L', "Bout./flac. en plastique > 8L",
           "Emballage métallique", "Huile de friture", "Carton à boisson", "Papier-carton",
           "Verre incolore", "Verre coloré", "FR", "PDP", "DSM")
        annotations = "/home/orumfels/datasets/triscandata/annotations/instances_"
        folder = "/home/orumfels/datasets/triscandata/"
    # annotations = "/home/orumfels/datasets/small_triscan/annotations/instances_"
    # folder = "/home/orumfels/datasets/small_triscan/"
    # 

    current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    if args.data_augment :
        print("Data augmenting")
        # path of the tensorboard writer
        log_dir = 'logs/data_augment/' + current_time
        # path to save the models
        save_path = "work_dir/data_augment/faster_rcnn_resnet50_"
        train_dataset = CustomDataset(folder+"train/", classes, annotations+"train.json", True, args.background)
    else :
        print("No data augment")
        # path of the tensorboard writer
        log_dir = 'logs/' + current_time
        # path to save the models
        save_path = "work_dir/faster_rcnn_resnet50_"
        train_dataset = CustomDataset(folder+"train/", classes, annotations+"train.json", False, args.background)

    test_dataset = CustomDataset(folder+"/val/", classes, annotations+"val.json", False, args.background)
    train_summary_writer = tf.summary.create_file_writer(log_dir)

    # define training and validation data loaders
    data_loader = torch.utils.data.DataLoader(train_dataset, batch_size=3, shuffle=True,
                                              num_workers=4,collate_fn=utils.collate_fn)
    data_loader_test = torch.utils.data.DataLoader(test_dataset, batch_size=1, shuffle=False,
                                                   num_workers=4, collate_fn=utils.collate_fn)
    print(" {} are training and {} testing".format(train_dataset.__len__(), test_dataset.__len__() ))
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if args.background:
        num_classes = len(classes) +1
        class_range = range(1, len(classes))
    else :
        num_classes = len(classes)
        class_range = range(0, len(classes))
    # get the model using our helper function
    model = get_model(num_classes, args.pretrain_coco, args.pretrain_back, args.custom_anchors)
    # move model to the right device
    model.to(device)
    # construct an optimizer
    params = [p for p in model.parameters() if p.requires_grad]
    optimizer = torch.optim.Adam(params, lr=0.0001) # momentum=0.9, weight_decay=0.0005)
    #optimizer = torch.optim.SGD(params, lr=0.00001, momentum=0.9, weight_decay=0.0005)
    # and a learning rate scheduler which decreases the learning rate by # 10x every 3 epochs
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer,step_size=10,gamma=0.1)
    print("Beginning training")
    for epoch in range(args.epochs):
        # train for one epoch, printing every 1000 iterations
        loss = train_one_epoch(model, optimizer, data_loader, device, epoch,print_freq=1000)
        test_loss = test(model, optimizer, data_loader_test, device, epoch,print_freq=1000)
        # update the learning rate
        lr_scheduler.step()
        average_precisions, precisions, recalls, IOU, top_1_accuracy = evaluate_map(test_dataset, model, data_loader_test, device, score_threshold=args.score_thres)
        mAP = []
        for label in class_range:
            avg = average_precisions[label][0]
            mAP.append(avg)
        with train_summary_writer.as_default():
            tf.summary.scalar('mAP', np.mean(np.asarray(mAP)), step=epoch+1)
            tf.summary.scalar('Precision', np.mean(np.asarray(precisions)), step=epoch+1)
            tf.summary.scalar('Recalls', np.mean(np.asarray(recalls)), step=epoch+1)
            tf.summary.scalar('Loss', np.mean(loss), step=epoch+1)
            tf.summary.scalar('Test Loss', np.mean(test_loss), step=epoch+1)
            tf.summary.scalar('IOU', IOU, step=epoch+1)
            tf.summary.scalar('Top-1 accuracy', top_1_accuracy, step=epoch+1)
        if epoch % 2 == 0 :
            # save model
            torch.save(model.state_dict(), save_path+str(epoch)+"_0001anchors_"+ str(len(classes)) + 'classes.pth')
    print("Test loss")
if __name__ == '__main__':
    main()
