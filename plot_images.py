import json
import cv2
# import tfe_my_implementations.my_resnet.utils as utils
import tfe_my_implementations.pytorch-retinanet.retinanet.dataloader as ret_data
import numpy as np
import torch

def draw_text(dataset, save_folder):
    classes = dataset.get_classes()
    #switch to evaluation mode
    # for all images in the test dataset
    done_class = np.zeros(12)
    for i in range(0, dataset.__len__()):
        filename = dataset.get_filename(i)
        x, y = dataset.__getitem__(i)
        if done_class[y] < 4 :
            done_class[y] = done_class[y] + 1
            # open image and put label on it
            image = cv2.imread(filename)
            text = classes[y]
            #image = cv2.putText(image, text, (100, 500), cv2.FONT_HERSHEY_SIMPLEX ,
            #10, (255, 255, 255), 10, cv2.LINE_AA) # font scale lowered
            name = save_folder + "class"+str(y)+ "_" + str(i) + ".jpg"
            print("Writing image, ", name)
            saved = cv2.imwrite(name, image)
            print("saved ?", saved)
def draw_bbox(dataset, save_folder):
    classes = dataset.get_classes()
    #switch to evaluation mode
    # for all images in the test dataset
    done_class = np.zeros(12)
    for i in range(0, dataset.__len__()):
        filename = dataset.get_filename(i)
        sample = dataset.__getitem__(i)
        image = sample['img']
        annot = sample['annot']
        new_sample = ret_data.transform_data(image, annot)
        new_image = new_sample['img']
        new_annot = new_sample['annot']
        if done_class[y] < 4 :
            done_class[y] = done_class[y] + 1
            text = classes[y]
            #image = cv2.putText(image, text, (100, 500), cv2.FONT_HERSHEY_SIMPLEX ,
            #10, (255, 255, 255), 10, cv2.LINE_AA) # font scale lowered
            image = cv2.rectangle(image, (annot[0,0], annot[0,1]), (annot[0,2], annot[0,3]), (0, 255, 0), 2)
            new_image = cv2.rectangle(new_image, (new_annot[0,0], new_annot[0,1]), (new_annot[0,2], new_annot[0,3]), (0, 255, 0), 2)
            name = save_folder + "class"+str(y)+ "_" + str(i) + ".jpg"
            new_name = save_folder + "class"+str(y)+ "_" + str(i) + "_box.jpg"
            print("Writing image, ", name)
            saved = cv2.imwrite(name, image)
            saved = cv2.imwrite(new_name, new_image)
            print("saved ?", saved)
classes = ('Aerosol','Bout./flac. en plastique < 8L', "Bout./flac. en plastique > 8L",
           "Emballage metallique", "Huile de friture", "Carton a boisson", "Papier-carton",
           "Verre incolore", "Verre colore", "FR", "PDP", "DSM")
dataset = ret_data.CustomDataset("/home/orumfels/datasets/triscandata/train/",
                              classes,
                              "/home/orumfels/datasets/triscandata/annotations/instances_train.json")
draw_bbox(dataset, "/home/orumfels/datasets/samples/")
