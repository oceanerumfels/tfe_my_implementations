import os
import torch
from PIL import Image
from torch.utils.data import Dataset
import json
import cv2
from progressbar import ProgressBar
import torchvision.transforms as transforms
import numpy as np

def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        if filename.endswith('.jpg'):
            images.append(filename)
    return images

def create_list(dataset):
    #open json file for labels
    f = open (dataset.json_file, "r")
    data = json.loads(f.read())
    f.close()
    if os.path.isfile(dataset.folder):
        filenames = [dataset.folder.split('/')[-1]]
    else :
        # Get the filenames of all training images
        filenames = load_images_from_folder(dataset.folder)
    labels = []
    images = []
    for fn in filenames:
        found_index = None
        for j, x in enumerate(data['images']):
            if x['filename'] == fn :
                found_index = j
                break
        #i is the index of found image
        if found_index is not None : #make sure annotation exists
            annot = data['annotations'][found_index]
            labels.append(annot['category_id'] - 1) #integer
            if os.path.isfile(dataset.folder):
                images.append(dataset.folder)
            else:
                images.append(dataset.folder + fn)
    return images, labels

class CustomDataset(Dataset):
    """The training table dataset.
    """
    def __init__(self, folder, classes, json_file, data_augment):
        #print("init with ", folder)
        self.json_file = json_file
        self.folder = folder
        self.classes = classes
        filenames, labels = create_list(self)
        self.filenames =  filenames
        self.labels = labels
        self.len = len(self.filenames) # Size of data
        self.augment = data_augment
        # define transformation
        self.transform = transforms.Compose([transforms.Resize((300, 300)),
                                             transforms.RandomHorizontalFlip(),
                                             transforms.RandomVerticalFlip(),
                                             transforms.ToTensor(),
                                             transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))]) # mean and std value same as ImageNet
        self.basic_transform = transforms.Compose([transforms.Resize((300, 300)),
                                             transforms.ToTensor(),
                                             transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))])

    def __getitem__(self, index):
        # source : https://towardsdatascience.com/building-your-own-object-detector-pytorch-vs-tensorflow-and-how-to-even-get-started-1d314691d4ae
        img_name = self.filenames[index]
        image = Image.open(img_name).convert("RGB")
        if self.augment :
            tensor_image = self.transform(image)
        else :
            tensor_image = self.basic_transform(image)
        
        return tensor_image, self.labels[index]

    def __len__(self):
        return self.len

    def get_filename(self, index):
        return self.filenames[index]
    
    def get_classes(self):
        return self.classes

#show results
def show_classification_results(dataset, model, result_folder):
    classes = dataset.get_classes()
    #switch to evaluation mode
    model.eval()
    # for all images in the test dataset
    for i in range(0, dataset.__len__()):
        filename = dataset.get_filename(i)
        x, y = dataset.__getitem__(i)
        shape = x.shape
        # create tensor with image
        tmp = torch.zeros(1, shape[0], shape[1], shape[2])
        tmp [0] = x
        # get predicted results
        output = model(tmp)
        output = torch.argmax(output).item()
        # open image and put label on it
        image = cv2.imread(dataset.folder+filename)
        text = "predicted label = " + classes[output]
        image = cv2.putText(image, text, (10, 50), cv2.FONT_HERSHEY_SIMPLEX ,  
                   1, (255, 0, 0), 2, cv2.LINE_AA)
        text = " real label = " + classes[y]
        image = cv2.putText(image, text, (10, 100), cv2.FONT_HERSHEY_SIMPLEX ,  
                   0.8, (255, 0, 0), 2, cv2.LINE_AA) # font scale lowered
        cv2.imwrite(result_folder+filename, image)
def get_weights(dataset):
    weights = []
    occurences = np.zeros(12)
    print("Sampling")
    for i in range(len(dataset)):
        img, label = dataset[i]
        occurences[label] += 1
    for i in range(len(dataset)):
        img, label = dataset[i]
        weights.append(1/occurences[label])
    return weights