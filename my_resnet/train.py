import torchvision.transforms as transforms
import torch
from torch.utils.data import DataLoader
import os
import argparse
import torch.nn as nn
import cv2
import torch.optim as optim
import numpy as np
from barbar import Bar
from sklearn.metrics import accuracy_score
import tensorflow as tf
import datetime
import statistics 
#my codes
from utils import CustomDataset, get_weights
from resnet import resnet18, resnet50, resnet101, resnet152


def parse_args():
    parser = argparse.ArgumentParser(description='Train a resnet model')
    parser.add_argument('resnet_model', default = 'resnet18' ,help='type of model')
    parser.add_argument('root_folder', default = "/home/orumfels/datasets/small_triscan/", help='dir that contains the data')
    parser.add_argument('work_dir', help='dir that contains the save of the models')
    parser.add_argument('--epochs', type=int, default=10, help='number of epochs')
    parser.add_argument('--save_rate', type=int, default=5, help='rate at which a model must be saved')
    parser.add_argument('--validation_rate', type=int, default=None, help='rate at which a model must be saved')
    parser.add_argument('--dataset_name', default='triscan', help='Name of the dataset')
    parser.add_argument('--data_augment', action='store_true', help='set this flag to enable data augmentation')
    parser.add_argument('--data_balancing', action='store_true', help='set this flag to enable class balancing')
    args = parser.parse_args()
    return args
    
dispatcher={'resnet18': resnet18,
            'resnet50' : resnet50,
            'resnet101': resnet101,
            'resnet152': resnet152}
                        
def main():
    # Get user parameters
    args = parse_args()
    folder_name, work_dir = args.root_folder, args.work_dir
    json_file = folder_name + "annotations/instances_train.json"
    val_json_file = folder_name + "annotations/instances_val.json"
    val_folder = folder_name+"val/"
    folder_name += "train/"
    num_epochs = args.epochs
    save_rate, val_rate = args.save_rate, args.validation_rate
    print(args.data_augment, args.data_balancing)
    # Get specified resnet model
    try:
        function=dispatcher[args.resnet_model]
    except KeyError:
        raise ValueError('invalid model')
    # set device for progress bar
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    # set tensorboard directory
    current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

    model = function(pretrained = True, progress = True).to(device) #3 = nb channels, 12 = nb classes

    # Build datasets
    classes = ('Aerosol','Bout./flac. en plastique < 8L', "Bout./flac. en plastique > 8L",
           "Emballage métallique", "Huile de friture", "Carton à boisson", "Papier-carton",
           "Verre incolore", "Verre coloré", "FR", "PDP", "DSM")
    if args.data_augment:
        print("Data augmentation set")
        dataset = CustomDataset(folder_name, classes, json_file, True)
        path = path = work_dir+args.dataset_name+'/'+args.resnet_model +"/data_augment/"
        train_log_dir = 'logs/' + args.dataset_name + '/' +args.resnet_model +"/data_augment/"
    else :
        dataset = CustomDataset(folder_name, classes, json_file, False)
        path = path = work_dir+args.dataset_name+'/'+args.resnet_model +"/"
        train_log_dir = 'logs/' + args.dataset_name + '/' +args.resnet_model + '/'
    if args.data_balancing:
        print("Data balancing set")
        weights = get_weights(dataset)
        
        sampler = torch.utils.data.WeightedRandomSampler(weights, len(dataset), replacement=True, generator=None)
        print("Dataloader ...")
        trainloader = torch.utils.data.DataLoader(dataset, num_workers=2, sampler = sampler, batch_size=4 ) #shuffle=True, batch_size=4)
        path = path + "data_balancing_epoch_"
        train_log_dir += '_data_balancing_'
    else :
        trainloader = torch.utils.data.DataLoader(dataset, num_workers=2, shuffle=True, batch_size=4)
    
    if val_rate is not None :
        print("folder validation =", val_folder)
        val_dataset = CustomDataset(val_folder, classes, val_json_file, False)
        testloader = torch.utils.data.DataLoader(val_dataset, batch_size=1, shuffle=True, num_workers=2)
    
    train_summary_writer = tf.summary.create_file_writer(train_log_dir+str(current_time))

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=0.0001 )#, momentum=0.9)
    
#--------------------------------------------------Training--------------------------------------------------#
    acc = None
    for epoch in range(num_epochs):
        tmp_loss = []
        print('Epoch: {}'.format(epoch+1))
        for idx, (x, y) in enumerate(Bar(trainloader)):
            # print progress
            x, y = x.to(device), y.to(device)
            # forward inputs
            outputs = model(x)
            # get loss
            loss = criterion(outputs, y)
            tmp_loss.append(loss.item())
            optimizer.zero_grad()
            # Calculate gradients (backpropogation)
            loss.backward()
            # Adjust parameters based on gradients
            optimizer.step()
            
#-----------------------------------------------Validation step-----------------------------------------------#
        # If requested
        if val_rate is not None and (epoch+1) % val_rate == 0:
            # swith model to eval (model will not learn)
            model.eval()
            with torch.no_grad():
                correct = 0
                total = 0
                for inputs, targets in testloader:
                    outputs = model(inputs.to(device))
                    loss = criterion(outputs, targets.to(device))
                    _, predicted = outputs.max(1)
                    # Add number of elements in batch
                    total += targets.size(0)
                    # sum of correctly predicted targets
                    correct += predicted.eq(targets.to(device)).sum().item()
                acc = 100*correct/total
            # switch back to train mode
            model.train()
            
#-------------------------------------------------Saving data-------------------------------------------------#

        with train_summary_writer.as_default():
            tf.summary.scalar('loss', np.mean(tmp_loss), step=epoch+1)
            if acc is not None : # if validation step, register it
                tf.summary.scalar('Accuracy on testing images', acc, step=epoch+1)
                acc = None
        print("Loss at {}th epoch: {}".format(epoch+1, np.mean(tmp_loss)))
        
        # save checkpoint model at chosen rate
        if (epoch+1) % save_rate == 0:
            torch.save(model.state_dict(), path + str(epoch+1)+ ".pth")

if __name__ == '__main__':
    main()