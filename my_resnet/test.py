import torch
from torch.utils.data import DataLoader
import os
import torch.nn as nn
import numpy as np
import cv2
import torch.optim as optim
#my codes
from utils import CustomDataset, show_classification_results, get_weights
from resnet import resnet18, resnet50, resnet101, resnet152
import matplotlib.pyplot as plt
import seaborn as sns
from progressbar import ProgressBar
from torchsummary import summary

import argparse

def parse_args():
    parser = argparse.ArgumentParser(description='Train a resnet model')
    parser.add_argument('--resnet_model', default = 'resnet18' ,help='type of model')
    parser.add_argument('--model', default = '/home/orumfels/tfe_my_implementations/my_resnet/models/epoch0' ,help='model to test')
    parser.add_argument('--test_folder', default = "/home/orumfels/datasets/small_triscan/", help='dir that contains the data')
    parser.add_argument('--result_folder', default = None, help='dir if we wish to see result on images')
    parser.add_argument('--data_augment', action='store_true', help='set this flag to enable data augmentation')
    parser.add_argument('--data_balancing', action='store_true', help='set this flag to enable class balancing')
    args = parser.parse_args()
    return args
    
dispatcher={'resnet18': resnet18,
            'resnet50' : resnet50,
            'resnet101': resnet101,
            'resnet152': resnet152}
                        
def main():
    args = parse_args()
    print(args)
    folder_name, model_path = args.test_folder, args.model
    json_file = folder_name + "annotations/instances_val.json"
    folder_name += "val/"
    # Get specified resnet model
    try:
        function=dispatcher[args.resnet_model]
    except KeyError:
        raise ValueError('invalid model')

    # Build dataset
    classes = ('Aerosol','Bout./flac. en plastique < 8L', "Bout./flac. en plastique > 8L",
           "Emballage métallique", "Huile de friture", "Carton à boisson", "Papier-carton",
           "Verre incolore", "Verre coloré", "FR", "PDP", "DSM")
    dataset = CustomDataset(folder_name, classes,json_file, args.data_augment)
    # check if must perform data balancing
    if args.data_balancing:
        print("Data balancing set")
        weights = get_weights(dataset)
        
        sampler = torch.utils.data.WeightedRandomSampler(weights, len(dataset), replacement=True, generator=None)
        testloader = torch.utils.data.DataLoader(dataset, num_workers=2, sampler = sampler, batch_size=4) #shuffle=True, batch_size=4)
    else :
        testloader = torch.utils.data.DataLoader(dataset, batch_size = 4, num_workers=2)
    
    # Load model
    model = function(3, 12) #3 = nb channels, 12 = nb classes
    if torch.cuda.is_available() :
        model.load_state_dict(torch.load(model_path))
    else :
        model.load_state_dict(torch.load(model_path, map_location=torch.device('cpu')))
    print(model)
    summary(model, (3, 300, 300))
    model.eval()
    criterion = nn.CrossEntropyLoss()
    confusion_matrix = np.zeros((len(classes), len(classes)))
    print("dataset len :", len(dataset))

#--------------------------------------------------Testing--------------------------------------------------#

    with torch.no_grad():
        correct = 0
        total = 0
        pbar = ProgressBar()
        for inputs, targets in pbar(testloader):
            outputs = model(inputs)
            loss = criterion(outputs, targets)
            _, predicted = outputs.max(1)
            total += targets.size(0)
            correct += predicted.eq(targets).sum().item()
            for i in range(len(inputs)) :
                confusion_matrix[int(targets[i]), int(predicted[i])] += 1
        print('Accuracy of the model on the testing images: %d %%' % (100 * correct / total))
        print(100* correct /total )
        print("confusion matrix : \n", confusion_matrix)
    for i in range(len(classes)):
        mask = np.ones(confusion_matrix.shape, bool)
        TP = confusion_matrix[i,i]
        mask[:,:] = False
        mask[:,i] = True
        mask[i,i] = False
        FP = np.sum(confusion_matrix[mask])
        mask[:,:] = False
        mask[i,:] = True
        mask[i,i] = False
        FN = np.sum(confusion_matrix[mask])
        mask[:,:] = True
        mask[i, :] = False
        mask[:, i] = False
        for j in range(len(classes)):
            # exclude line i, column i and diagonal
            mask[j,j] = False
        TN = np.sum(confusion_matrix[mask])
        print("class ", i)
        print("TP, TN, FP, FN = ", TP, TN, FP, FN)
        print("accuracy =", (TP+TN) / (TP + TN + FP + FN))
        print("precision = ",TP / (TP + FP))
        print("recall =", TP / (TP + FN))
            
        print("Accuracy ",i, " = ", confusion_matrix[i])
    categories = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
    fig = plt.figure()
    ax = fig.add_subplot(111)
        
    sns.set(font_scale=0.5) # Adjust to fit
    sns.heatmap(confusion_matrix, annot=True, ax = ax, cmap="Blues", xticklabels=categories,yticklabels=categories)
    cax = ax.matshow(confusion_matrix)
    
    ax.set_title('Confusion matrix - '+ args.resnet_model)
    
    #ax.set_xticklabels([''] + labels)
    #ax.set_yticklabels([''] + labels)
    plt.xlabel('Predicted labels')
    plt.ylabel('True labels')
    plt.savefig('CM-50-130.png')
    if args.result_folder is not None :
        # Draw result on images
        show_classification_results(dataset, model, args.result_folder)
if __name__ == '__main__':
    main()
