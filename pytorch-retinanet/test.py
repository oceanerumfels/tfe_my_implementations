import argparse
import collections

import numpy as np

import torch
import torch.optim as optim
from torchvision import transforms

from retinanet import model
from retinanet.dataloader import CocoDataset, CustomDataset, collater, Resizer, Augmenter, Normalizer
from torch.utils.data import DataLoader

from retinanet import coco_eval
from retinanet import csv_eval

assert torch.__version__.split('.')[0] == '1'

print('CUDA available: {}'.format(torch.cuda.is_available()))


def main(args=None):

    parser = argparse.ArgumentParser(description='Simple training script for training a RetinaNet network.')

    parser.add_argument('--dataset', help='Dataset type, must be one of csv or coco.')
    parser.add_argument('--json', help='Path to file containing all images and annotations (see readme)')
    parser.add_argument('--weights', help = 'weights location')

    parser.add_argument('--depth', help='Resnet depth, must be one of 18, 34, 50, 101, 152', type=int, default=50)
    parser.add_argument('--data_augment', action='store_true', help='set this flag to enable data augmentation')
    parser.add_argument('--data_balancing', action='store_true', help='set this flag to enable class balancing')
    

    parser = parser.parse_args(args)
    
    # set tensorboard directory

    # Create the data loaders
    if parser.dataset == 'json':
        if parser.json is None:
            raise ValueError('Must provide --csv_train when training custom data,')
        class_list = ['Aerosol','Bout./flac. en plastique < 8L', "Bout./flac. en plastique > 8L",
           "Emballage métallique", "Huile de friture", "Carton à boisson", "Papier-carton",
           "Verre incolore", "Verre coloré", "FR", "PDP", "DSM"]
        if parser.data_augment :
            print("\n DATA AUGMENT SET \n")
            dataset_val = CustomDataset(root_dir=parser.json, class_list=class_list,
                                   transform=transforms.Compose([Normalizer(), Resizer()]), train = False, data_augment = True)
        else :
            print("\n NO DATA AUGMENT \n")
            dataset_val = CustomDataset(root_dir=parser.json, class_list=class_list,
                                   transform=transforms.Compose([Normalizer(), Resizer()]), train = False, data_augment = False)
        

    else:
        raise ValueError('Dataset type not understood (must be json), exiting.')
    
    if parser.data_balancing :
        print("DATA BALANCING SET")
        # Determine weights for weight sampling
        weights = []
        occurences = np.zeros(dataset_val.num_classes())
        print("Sampling")
        for i in range(len(dataset_val)):
            data = dataset_val.load_annotations(i)
            label = int(data[0, 4])
            occurences[label] = occurences[label] + 1
        for i in range(len(dataset_val)):
            data = dataset_val.load_annotations(i)
            label = int(data[0, 4])
            weights.append(1/occurences[label])
        
        sampler = torch.utils.data.WeightedRandomSampler(weights, len(dataset_val) * 2, replacement=True, generator=None)
        dataloader_val = DataLoader(dataset_val, num_workers=0, collate_fn=collater,  sampler=sampler, batch_size=5)
    else :
        dataloader_val = DataLoader(dataset_val, num_workers=0, collate_fn=collater, batch_size=1)

    # Create the model
    model_loaded = False
    if parser.weights is None :
        if parser.depth == 18:
            retinanet = model.resnet18(num_classes=dataset_train.num_classes(), pretrained=True)
        elif parser.depth == 34:
            retinanet = model.resnet34(num_classes=dataset_train.num_classes(), pretrained=True)
        elif parser.depth == 50:
            retinanet = model.resnet50(num_classes=dataset_train.num_classes(), pretrained=True)
        elif parser.depth == 101:
            retinanet = model.resnet101(num_classes=dataset_train.num_classes(), pretrained=True)
        elif parser.depth == 152:
            retinanet = model.resnet152(num_classes=dataset_train.num_classes(), pretrained=True)
        else:
            raise ValueError('Unsupported model depth, must be one of 18, 34, 50, 101, 152')
    else :
        print("Loading model ...")
        if torch.cuda.is_available() :
            retinanet = torch.load(parser.weights)
        else :
            retinanet = torch.load(parser.weights, map_location=torch.device('cpu'))
        print("model loaded")
        model_loaded = True

    use_gpu = True

    if use_gpu:
        if torch.cuda.is_available():
            retinanet = retinanet.cuda()

    if torch.cuda.is_available():
        retinanet = torch.nn.DataParallel(retinanet).cuda()
    else:
        retinanet = torch.nn.DataParallel(retinanet)

    retinanet.training = False

    retinanet.eval()
    
    print('Num validation images: {}'.format(len(dataset_val)))
    average_precisions, precisions, recalls, top_1_accuracy = csv_eval.evaluate(dataset_val, retinanet)
    mAP = []
    for label in range(0, dataset_val.num_classes()):
        avg = average_precisions[label][0] 
        mAP.append(avg)
    print("MAP = ", np.mean(np.asarray(mAP)))

    torch.save(retinanet, 'model_final.pt')


if __name__ == '__main__':
    main()

