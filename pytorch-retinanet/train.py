import argparse
import collections

import numpy as np

import torch
import torch.optim as optim
from torchvision import transforms

from retinanet import model
from retinanet.dataloader import CocoDataset, CustomDataset, collater, Resizer, Augmenter, Normalizer
from torch.utils.data import DataLoader

from retinanet import coco_eval
from retinanet import csv_eval
import tensorflow as tf # version 2
import datetime
import time

assert torch.__version__.split('.')[0] == '1'

print('CUDA available: {}'.format(torch.cuda.is_available()))


def main(args=None):

    parser = argparse.ArgumentParser(description='Simple training script for training a RetinaNet network.')

    parser.add_argument('--dataset', help='Dataset type, must be one of csv or coco.')
    parser.add_argument('--json', help='Path to file containing all images and annotations (see readme)')
    parser.add_argument('--work_dir', help='Path to save model')
    parser.add_argument('--log_dir', help = 'Name of log dir')
    parser.add_argument('--weights', help = 'weights location')

    parser.add_argument('--depth', help='Resnet depth, must be one of 18, 34, 50, 101, 152', type=int, default=50)
    parser.add_argument('--epochs', help='Number of epochs', type=int, default=150)
    parser.add_argument('--previous_epoch_count', help = 'where we start the training again', type = int)
    parser.add_argument('--data_augment', action='store_true', help='set this flag to enable data augmentation')
    parser.add_argument('--data_balancing', action='store_true', help='set this flag to enable class balancing')
    

    parser = parser.parse_args(args)
    
    # set tensorboard directory
    current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    train_log_dir = 'logs/'+ parser.log_dir + current_time
    train_summary_writer = tf.summary.create_file_writer(train_log_dir)
    
    save_path = parser.work_dir+'/'+str(parser.depth)+'/'

    # Create the data loaders
    if parser.dataset == 'json':
        if parser.json is None:
            raise ValueError('Must provide --csv_train when training custom data,')
        class_list = ['Aerosol','Bout./flac. en plastique < 8L', "Bout./flac. en plastique > 8L",
           "Emballage métallique", "Huile de friture", "Carton à boisson", "Papier-carton",
           "Verre incolore", "Verre coloré", "FR", "PDP", "DSM"]
        if parser.data_augment :
            print("\n DATA AUGMENT SET \n")
            dataset_train = CustomDataset(root_dir=parser.json, class_list=class_list,
                                   transform=transforms.Compose([Normalizer(), Augmenter(), Resizer()]), train = True, data_augment = True)
            save_path = save_path + 'data_augment/'
        else :
            print("\n NO DATA AUGMENT \n")
            dataset_train = CustomDataset(root_dir=parser.json, class_list=class_list,
                                   transform=transforms.Compose([Normalizer(), Augmenter(), Resizer()]), train = True, data_augment = False)
        dataset_val = CustomDataset(root_dir=parser.json, class_list=class_list,
                                   transform=transforms.Compose([Normalizer(), Resizer()]), train = False, data_augment = False)

    else:
        raise ValueError('Dataset type not understood (must be csv or coco), exiting.')
    
    if parser.data_balancing :
        print("DATA BALANCING SET")
        # Determine weights for weight sampling
        weights = []
        occurences = np.zeros(CustomDataset.num_classes())
        print("Sampling")
        for i in range(len(dataset_train)):
            data = dataset_train.load_annotations(i)
            label = int(data[0, 4])
            occurences[label] = occurences[label] + 1
        for i in range(len(dataset_train)):
            data = dataset_train.load_annotations(i)
            label = int(data[0, 4])
            weights.append(1/occurences[label])
        
        sampler = torch.utils.data.WeightedRandomSampler(weights, len(dataset_train) * 2, replacement=True, generator=None)
        dataloader_train = DataLoader(dataset_train, num_workers=0, collate_fn=collater,  sampler=sampler, batch_size=5)
        save_path = save_path + "data_balancing_"
    else :
        dataloader_train = DataLoader(dataset_train, num_workers=0, collate_fn=collater, batch_size=3)

    if dataset_val is not None:
        print("DataLoader val ...")
        dataloader_val = DataLoader(dataset_val, num_workers=0, collate_fn=collater, batch_size=3) # batch_sampler=sampler_val)

    # Create the model
    model_loaded = False
    if parser.weights is None :
        if parser.depth == 18:
            retinanet = model.resnet18(num_classes=dataset_train.num_classes(), pretrained=True)
        elif parser.depth == 34:
            retinanet = model.resnet34(num_classes=dataset_train.num_classes(), pretrained=True)
        elif parser.depth == 50:
            retinanet = model.resnet50(num_classes=dataset_train.num_classes(), pretrained=True)
        elif parser.depth == 101:
            retinanet = model.resnet101(num_classes=dataset_train.num_classes(), pretrained=True)
        elif parser.depth == 152:
            retinanet = model.resnet152(num_classes=dataset_train.num_classes(), pretrained=True)
        else:
            raise ValueError('Unsupported model depth, must be one of 18, 34, 50, 101, 152')
    else :
        print("Loading model ...")
        retinanet = torch.load(parser.weights)
        print("model loaded")
        model_loaded = True

    use_gpu = True

    if use_gpu:
        if torch.cuda.is_available():
            retinanet = retinanet.cuda()

    if torch.cuda.is_available():
        retinanet = torch.nn.DataParallel(retinanet).cuda()
    else:
        retinanet = torch.nn.DataParallel(retinanet)

    retinanet.training = True

    optimizer = optim.Adam(retinanet.parameters(), lr=1e-5)

    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, patience=3, verbose=True)

    loss_hist = collections.deque(maxlen=500)

    retinanet.train()
    retinanet.module.freeze_bn()

    print('Num training images: {}'.format(len(dataset_train)))

    for epoch in range(parser.epochs):
        start = time.time()

        retinanet.train()
        retinanet.module.freeze_bn()

        epoch_loss = []
        reg_loss = []
        class_loss = []
        for iter_num, data in enumerate(dataloader_train):
            try:
                optimizer.zero_grad()

                if torch.cuda.is_available():
                    classification_loss, regression_loss = retinanet([data['img'].cuda().float(), data['annot']])
                else:
                    classification_loss, regression_loss = retinanet([data['img'].float(), data['annot']])
                    
                classification_loss = classification_loss.mean()
                regression_loss = regression_loss.mean()
                reg_loss.append(regression_loss.item())
                class_loss.append(classification_loss.item())

                loss = classification_loss + regression_loss

                if bool(loss == 0):
                    continue
                middle = time.time()

                loss.backward()

                torch.nn.utils.clip_grad_norm_(retinanet.parameters(), 0.1)

                optimizer.step()

                loss_hist.append(float(loss))

                epoch_loss.append(float(loss))

                print('Epoch: {} | Iteration: {} | Classification loss: {:1.5f} | Regression loss: {:1.5f} | Running loss:{:1.5f}'.format(epoch, iter_num, float(classification_loss), float(regression_loss), np.mean(loss_hist)))
                

                del classification_loss
                del regression_loss
            except Exception as e:
                print(e)
                continue
        end = time.time()
        print("epoch time :", end - start)
        if (parser.dataset =='json') and (epoch % 2 == 0):
            mAP = []
            print('Evaluating dataset')
            # returns a dictionary
            average_precisions, precisions, recalls, top_1_accuracy = csv_eval.evaluate(dataset_val, retinanet)
            for label in range(0, dataset_train.num_classes()):
                avg = average_precisions[label][0]
                mAP.append(avg)
            print("MAP :", np.mean(np.asarray(mAP)))
            print("precisions :", np.mean(np.asarray(precisions)))
            print("recalls :", np.mean(np.asarray(recalls)))
            
        else :
            print("Not json dataset")
        if model_loaded :
            nb = epoch + 1 + parser.previous_epoch_count
        else :
            nb = epoch + 1
        with train_summary_writer.as_default():
            tf.summary.scalar('Training regression loss', np.mean(reg_loss) , step=nb)
            tf.summary.scalar('Training classification loss', np.mean(np.asarray(class_loss)), step=nb)
            tf.summary.scalar('Training loss', np.mean(np.asarray(epoch_loss)), step=nb)
            if mAP is not None :
                tf.summary.scalar('mAP', np.mean(np.asarray(mAP)), step=nb)
                tf.summary.scalar('Mean precisions', np.mean(np.asarray(precisions)), step=nb)
                tf.summary.scalar('Mean recalls', np.mean(np.asarray(recalls)), step=nb)
                tf.summary.scalar('Top-1 accuracy', top_1_accuracy, step=nb)
                mAP = None
        scheduler.step(np.mean(epoch_loss))
        work_dir = parser.work_dir
        torch.save(retinanet.module, save_path + str(nb) + ".pt")

    retinanet.eval()

    torch.save(retinanet, 'model_final.pt')


if __name__ == '__main__':
    main()
