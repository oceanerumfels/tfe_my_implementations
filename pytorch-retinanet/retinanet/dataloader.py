from __future__ import print_function, division
import sys
import os
import torch
import numpy as np
import random
import csv

from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
from torch.utils.data.sampler import Sampler

from pycocotools.coco import COCO

import skimage.io
import skimage.transform
import skimage.color
import skimage
from progressbar import ProgressBar
import random

from PIL import Image

import json


class CocoDataset(Dataset):
    """Coco dataset."""

    def __init__(self, root_dir, set_name='train2017', transform=None):
        """
        Args:
            root_dir (string): COCO directory.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.root_dir = root_dir
        self.set_name = set_name
        self.transform = transform

        self.coco      = COCO(os.path.join(self.root_dir, 'annotations', 'instances_' + self.set_name + '.json'))
        self.image_ids = self.coco.getImgIds()

        self.load_classes()

    def load_classes(self):
        # load class names (name -> label)
        categories = self.coco.loadCats(self.coco.getCatIds())
        categories.sort(key=lambda x: x['id'])

        self.classes             = {}
        self.coco_labels         = {}
        self.coco_labels_inverse = {}
        for c in categories:
            self.coco_labels[len(self.classes)] = c['id']
            self.coco_labels_inverse[c['id']] = len(self.classes)
            self.classes[c['name']] = len(self.classes)

        # also load the reverse (label -> name)
        self.labels = {}
        for key, value in self.classes.items():
            self.labels[value] = key

    def __len__(self):
        return len(self.image_ids)

    def __getitem__(self, idx):

        img = self.load_image(idx)
        annot = self.load_annotations(idx)
        sample = {'img': img, 'annot': annot}
        if self.transform:
            sample = self.transform(sample)

        return sample

    def load_image(self, image_index):
        image_info = self.coco.loadImgs(self.image_ids[image_index])[0]
        path       = os.path.join(self.root_dir, 'images', self.set_name, image_info['file_name'])
        img = skimage.io.imread(path)

        if len(img.shape) == 2:
            img = skimage.color.gray2rgb(img)

        return img.astype(np.float32)/255.0

    def load_annotations(self, image_index):
        # get ground truth annotations
        annotations_ids = self.coco.getAnnIds(imgIds=self.image_ids[image_index], iscrowd=False)
        annotations     = np.zeros((0, 5))

        # some images appear to miss annotations (like image with id 257034)
        if len(annotations_ids) == 0:
            return annotations

        # parse annotations
        coco_annotations = self.coco.loadAnns(annotations_ids)
        for idx, a in enumerate(coco_annotations):

            # some annotations have basically no width / height, skip them
            if a['bbox'][2] < 1 or a['bbox'][3] < 1:
                continue

            annotation        = np.zeros((1, 5))
            annotation[0, :4] = a['bbox']
            annotation[0, 4]  = self.coco_label_to_label(a['category_id'])
            annotations       = np.append(annotations, annotation, axis=0)

        # transform from [x, y, w, h] to [x1, y1, x2, y2]
        annotations[:, 2] = annotations[:, 0] + annotations[:, 2]
        annotations[:, 3] = annotations[:, 1] + annotations[:, 3]

        return annotations

    def coco_label_to_label(self, coco_label):
        return self.coco_labels_inverse[coco_label]


    def label_to_coco_label(self, label):
        return self.coco_labels[label]

    def image_aspect_ratio(self, image_index):
        image = self.coco.loadImgs(self.image_ids[image_index])[0]
        return float(image['width']) / float(image['height'])

    def num_classes(self):
        return 80

class CustomDataset(Dataset):
    """Custom dataset."""

    def __init__(self, root_dir, class_list, set_name='triscan', transform=None, train = True, data_augment = False):
        """
        Args:
            root_dir (string): COCO directory.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.tmp = root_dir
        root_dir = "/home/orumfels/datasets/triscandata"
        if train :
            self.root_dir = root_dir+"/train/"
            self.json_file = root_dir+"/annotations/instances_train.json"
            self.set_name = set_name+'_train'
        else :
            self.root_dir = root_dir+"/val/"
            self.json_file = root_dir+"/annotations/instances_val.json"
            self.set_name = set_name+'_val'
        self.transform = transform
        self.classes = class_list
        self.load_imgIDs_and_annotations()
        self.augment = data_augment
        

    def load_imgIDs_and_annotations(self):
        self.annotations = []
        filenames = []
        image_names = []
        f = open(self.json_file, "r")
        data = json.loads(f.read())
        f.close()
        if os.path.isfile(self.root_dir + self.tmp):
            image_names.append(self.tmp.split('/')[-1])
        else :
            image_names = os.listdir(self.root_dir)
        # load all the filenames
        # root_dir is a directory containing the file names
        for filename in image_names:
            if filename.endswith('.jpg'):
                # get index of corresponding annotation
                found_index = None
                for j,x in enumerate(data['images']):
                    if x['filename'] == filename:
                        found_index = j
                        break
                if found_index is not None:
                    self.annotations.append(data['annotations'][found_index])
                    label = data['annotations'][found_index]['category_id']-1
                    if os.path.isfile(self.tmp):
                        filenames.append(self.tmp)
                    else :
                        filenames.append(filename)
        self.filenames = filenames

    def __len__(self):
        return len(self.filenames)

    def __getitem__(self, idx):
        img = skimage.io.imread(self.root_dir+self.filenames[idx])
        im = img.astype(np.float32)/255.0
        annot = self.load_annotations(idx)
        # images type : np.float32/255.0
        # annotations : numpy array with [bbox label]
        if self.augment :
            sample = transform_data(im, annot, transform = self.transform)
        else :
            sample = {'img': im, 'annot': annot}
            if self.transform is not None :
                sample = self.transform(sample)
        # print("TRANSFORMATION :", sample['annot'])
        return sample

    def load_annotations(self, image_index):
        # get ground truth annotations
        a = self.annotations[image_index]
        annotation     = np.zeros((1, 5))
        annotation[0, :4] = a['bbox']
        annotation[0, 4]  = a['category_id']-1
        #annotation[0]       = annotation

        # transform from [x, y, w, h] to [x1, y1, x2, y2]
        annotation[:, 2] = annotation[:, 0] + annotation[:, 2]
        annotation[:, 3] = annotation[:, 1] + annotation[:, 3]

        return annotation

    def image_aspect_ratio(self, image_index):
        img = skimage.io.imread(self.root_dir+self.filenames[image_index])
        height = img.shape[0] # checked, it is correct
        width = img.shape[1]
        return float(width)/ float(height)
    def label_to_name(self, label):
        return self.classes[label]

    def num_classes(self):
        return len(self.classes)
    def get_classes(self):
        return self.classes
    def get_filename(self, image_index):
        return self.filenames[image_index]

def min_max(x, y, limx, limy):
    x = int(min(max(x, 0), limx))
    y = int(min(max(y, 0), limy))
    return x, y
def crop_center(img,cropx,cropy, annot, centrex, centrey):
    x = (centrex-cropx)/2
    y = (centrey-cropy)/2
    startx, starty = min_max(x, y, img.shape[0], img.shape[1])
    endx, endy = min_max(startx + cropx, starty+cropy, img.shape[0], img.shape[1])
    # pixels crop on the right
    right = img.shape[0] - (endx - startx)
    # pixels crop on top
    top = img.shape[1] - (endy - starty)
    annot2 = annot.copy()
    annot2[0,0], annot2[0,1] = min_max(annot[0,0] - startx, annot[0,1] - starty, endx-startx, endy-starty)
    annot2[0,2],annot2[0,3] = min_max(annot[0,2] - startx, annot[0,3] - starty, endx-startx, endy-starty)
    return img[starty:endy,startx:endx, :], annot2
    
def transform_data(image, init_annot, transform = None):
    """
    Input : 
    annot[0, :4] = bounding box [x1, y1, x2, y2]
    annot[0, 4] = label
    """
    height = image.shape[0]
    width = image.shape[1]
    annot = init_annot.copy()
    if random.choice([True, False]):
        # do a horizontal flip
        image = np.fliplr(image)
        # new bounding box
        x1 = width - annot[0, 2]
        x2 = width - annot[0, 0]
        annot[0, 0] = x1
        annot[0, 2] = x2
    if random.choice([True, False]):
        # do a vertical flip
        image = np.flipud(image)
        # new bounding box
        y1 = height - annot[0, 1]
        y2 = height - annot[0, 3]
        annot[0, 1] = y1
        annot[0, 3] = y2
    if random.choice([True, False]):
        # do a crop (square of 200 - 200)
        centers_list = [[annot[0, 0], annot[0,1]], [annot[0, 0], annot[0,3]],
                        [annot[0, 2], annot[0,1]], [annot[0, 2], annot[0,3]],
                        [(annot[0, 0]+annot[0, 2])//2, (annot[0, 1]+annot[0, 3])//2]]
        center_of_square = random.choice(centers_list)
        image, annot = crop_center(image, int(2*width/3), int(2*height/3), annot, center_of_square[0], center_of_square[1])
    sample = {'img': image, 'annot': annot}
    if transform is not None :
        sample = transform(sample)
    return sample
    

def collater(data):

    imgs = [s['img'] for s in data]
    annots = [s['annot'] for s in data]
    scales = [s['scale'] for s in data]
        
    widths = [int(s.shape[0]) for s in imgs]
    heights = [int(s.shape[1]) for s in imgs]
    batch_size = len(imgs)

    max_width = np.array(widths).max()
    max_height = np.array(heights).max()

    padded_imgs = torch.zeros(batch_size, max_width, max_height, 3)

    for i in range(batch_size):
        img = imgs[i]
        padded_imgs[i, :int(img.shape[0]), :int(img.shape[1]), :] = img

    max_num_annots = max(annot.shape[0] for annot in annots)
    
    if max_num_annots > 0:

        annot_padded = torch.ones((len(annots), max_num_annots, 5)) * -1

        if max_num_annots > 0:
            for idx, annot in enumerate(annots):
                #print(annot.shape)
                if annot.shape[0] > 0:
                    annot_padded[idx, :annot.shape[0], :] = annot
    else:
        annot_padded = torch.ones((len(annots), 1, 5)) * -1


    padded_imgs = padded_imgs.permute(0, 3, 1, 2)

    return {'img': padded_imgs, 'annot': annot_padded, 'scale': scales}

class Resizer(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample, min_side=608, max_side=1024):
        image, annots = sample['img'], sample['annot']

        rows, cols, cns = image.shape

        smallest_side = min(rows, cols)

        # rescale the image so the smallest side is min_side
        scale = min_side / smallest_side

        # check if the largest side is now greater than max_side, which can happen
        # when images have a large aspect ratio
        largest_side = max(rows, cols)

        if largest_side * scale > max_side:
            scale = max_side / largest_side

        # resize the image with the computed scale
        image = skimage.transform.resize(image, (int(round(rows*scale)), int(round((cols*scale)))))
        rows, cols, cns = image.shape

        pad_w = 32 - rows%32
        pad_h = 32 - cols%32

        new_image = np.zeros((rows + pad_w, cols + pad_h, cns)).astype(np.float32)
        new_image[:rows, :cols, :] = image.astype(np.float32)

        annots[:, :4] *= scale

        return {'img': torch.from_numpy(new_image), 'annot': torch.from_numpy(annots), 'scale': scale}


class Augmenter(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample, flip_x=0.5):

        if np.random.rand() < flip_x:
            image, annots = sample['img'], sample['annot']
            image = image[:, ::-1, :]

            rows, cols, channels = image.shape

            x1 = annots[:, 0].copy()
            x2 = annots[:, 2].copy()
            
            x_tmp = x1.copy()

            annots[:, 0] = cols - x2
            annots[:, 2] = cols - x_tmp

            sample = {'img': image, 'annot': annots}

        return sample


class Normalizer(object):

    def __init__(self):
        self.mean = np.array([[[0.485, 0.456, 0.406]]])
        self.std = np.array([[[0.229, 0.224, 0.225]]])

    def __call__(self, sample):

        image, annots = sample['img'], sample['annot']

        return {'img':((image.astype(np.float32)-self.mean)/self.std), 'annot': annots}

class UnNormalizer(object):
    def __init__(self, mean=None, std=None):
        if mean == None:
            self.mean = [0.485, 0.456, 0.406]
        else:
            self.mean = mean
        if std == None:
            self.std = [0.229, 0.224, 0.225]
        else:
            self.std = std

    def __call__(self, tensor):
        """
        Args:
            tensor (Tensor): Tensor image of size (C, H, W) to be normalized.
        Returns:
            Tensor: Normalized image.
        """
        for t, m, s in zip(tensor, self.mean, self.std):
            t.mul_(s).add_(m)
        return tensor