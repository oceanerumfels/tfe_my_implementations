
# import packages

import argparse
from retinanet.dataloader import CustomDataset, collater, Resizer, Augmenter, Normalizer
from torch.utils.data import DataLoader
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
import torch
from torchvision import transforms
import torchvision
import numpy as np
import cv2
import skimage.io
import skimage.transform
import skimage.color
import skimage
import os, sys, inspect
from matplotlib import pyplot as plt
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
import object_detection
from object_detection import my_utils
# from yolov5 import models
# from yolov5.models.experimental import attempt_load
from my_resnet import utils
from my_resnet import resnet
from my_resnet.resnet import resnet18, resnet50, resnet101, resnet152
import time
#from torchsummaryX import summary
from torchinfo import summary
import torchsummary as ts
# function definition

def add_boxes_to_image(real_box, real_label, pred_box, pred_label, path, image_name, algo_name, class_list):
    # Draw given boxes and labels on image
    image = cv2.imread(path)
    text_size = image.shape[0] / 600
    if text_size < 1:
        text_size = 1
    text_thickness = int(text_size * 3)
    image = cv2.rectangle(image, (int(real_box[0]), int(real_box[1])), (int(real_box[2]), int(real_box[3])),
                           (0, 255, 0), text_thickness)
    text = class_list[int(real_label)]
    image = cv2.putText(img = image, text = text, org = (int(real_box[0]), int(real_box[3] - 10 * text_size)), fontFace = cv2.FONT_HERSHEY_SIMPLEX ,
                     fontScale = text_size, color = (0, 255, 0), thickness = text_thickness, lineType = cv2.LINE_AA)
    image = cv2.rectangle(image, (int(pred_box[0]), int(pred_box[1])), (int(pred_box[2]), int(pred_box[3])), (255, 0, 0), text_thickness)
    text2 = class_list[int(pred_label)]
    print("real class = ", text, ", predicted class = ", text2)
    image = cv2.putText(img = image, text = text2, org = (int(pred_box[0]), int(pred_box[1] + 20 * text_size)), fontFace = cv2.FONT_HERSHEY_SIMPLEX ,
                     fontScale = text_size, color = (255, 0, 0), thickness = text_thickness, lineType = cv2.LINE_AA)
    #plt.imshow(image)
    #plt.show()
    cv2.imwrite("images/bboxes_"+algo_name+ "_"+image_name, image)

def get_faster_rcnn_model(num_classes):
    # load an object detection model pre-trained on COCO
    model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=False)
    # get the number of input features for the classifier
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    # replace the pre-trained head with a new on
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features,num_classes)
    return model

def load_retinanet(weight_path):
    model = torch.load(weight_path, map_location=torch.device('cpu'))
    model.eval()
    return model

def RetinaNet_inference(path, image_name, class_list, model):

    dataset = CustomDataset(image_name, class_list, transform=transforms.Compose([Normalizer(), Augmenter(), Resizer()]),
                        set_name='test', train = False, data_augment=False)

    # get detection
    data = dataset[0]
    start = time.time()
    scores, labels, boxes = model(data['img'].permute(2, 0, 1).float().unsqueeze(dim=0))

    inference_time = time.time() - start
    if len(scores.size()) == 0 :
        return inference_time
    max_index = torch.argmax(scores).numpy()
    # load image
    scale = data['scale']

    # load original box
    annot = data['annot'].detach().numpy()
    ori_boxes = annot[0, :4]
    ori_class = annot[0, 4]
    ori_boxes /=scale

    # Load predicted boxes
    new_box = boxes[max_index].detach().numpy()
    new_box /= scale
    new_label = labels[max_index].detach().numpy()
    add_boxes_to_image(ori_boxes, ori_class, new_box, new_label, path, image_name, "retinanet_", class_list)
    
    return inference_time

def load_faster(model_path, class_list):
    num_classes = len(class_list)
    model = get_faster_rcnn_model(num_classes)
    model.load_state_dict(torch.load(model_path, map_location=torch.device('cpu')))
    model.eval()
    return model

def Faster_inference(model, class_list, image_name, save_name):
    json_file = "/home/orumfels/datasets/triscandata/annotations/instances_val.json"
    dataset = my_utils.CustomDataset(image_name, class_list, json_file, data_augment = False, background=False)
    testloader = torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=False, num_workers=0)
    for inputs, targets in testloader:
        start = time.time()
        output = model(inputs)
        inference_time = time.time() - start
        scores = output[0]['scores']
        labels = output[0]['labels']
        boxes = output[0]['boxes']
        max_index = torch.argmax(scores).numpy()
        predicted_label = labels[max_index]
        predicted_box = boxes[max_index]
    # load original box
    img = cv2.imread(image_name)
    height, width, z = img.shape
    boxes = targets['boxes'].numpy()[0][0]
    boxes[0] = boxes[0] / (224/width)
    boxes[1] = boxes[1] / (224/height)
    boxes[2] = boxes[2] / (224/width)
    boxes[3] = boxes[3] / (224/height)
    label = targets['labels'].numpy()[0][0]

    # Load predicted boxes
    predicted_box = predicted_box.detach().numpy()
    predicted_box[0] = predicted_box[0] / (224/width)
    predicted_box[1] = predicted_box[1] / (224/height)
    predicted_box[2] = predicted_box[2] / (224/width)
    predicted_box[3] = predicted_box[3] / (224/height)
    predicted_label = predicted_label.numpy() -1
    add_boxes_to_image(boxes, label, predicted_box, predicted_label, image_name, save_name, "faster_", class_list)
    return inference_time


def load_resnet(model_path):
    model = resnet50(3, 12) #3 = nb channels, 12 = nb classes
    model.load_state_dict(torch.load(model_path, map_location=torch.device('cpu')))
    model.eval()
    return model
    
def Resnet_inference(model, path, class_list):
    json_file = "/home/orumfels/datasets/triscandata/annotations/instances_val.json"
    dataset = utils.CustomDataset(path, class_list, json_file, data_augment = False)
    testloader = torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=True, num_workers=0)
    for inputs, targets in testloader:
        start = time.time()
        outputs = model(inputs)
        inference_time = time.time() - start
        _, predicted = outputs.max(1)
        print("Real label =", class_list[int(targets.numpy())], ", predicted =", class_list[int(predicted.numpy())])
    return inference_time

def main(args = None):
    parser = argparse.ArgumentParser(description='Simple training script for training a RetinaNet network.')
    parser.add_argument('--nb_images', type=int, default=1, help='number of images to infer')
    parser = parser.parse_args(args)
    
    folder_path = "/home/orumfels/datasets/triscandata/val/"
    class_list = ['Aerosol','Bout./flac < 8L', "Bout./flac > 8L",
           "Emballage metallique", "Huile de friture", "Carton a boisson", "Papier-carton",
           "Verre incolore", "Verre colore", "FR", "PDP", "DSM"]

    

    # generate dataset with all images
    print("Building dataset ...")
    choice_dataset = CustomDataset(folder_path, class_list, transform=transforms.Compose([Normalizer(), Augmenter(), Resizer()]),
                        set_name='test', train = False, data_augment=False)
    
    # load all models
    weight_path = "/home/orumfels/tfe_my_implementations/pytorch-retinanet/work_dir/triscandata/50/data_augment/70.pt"
    retina_model = load_retinanet(weight_path)
    #print(retina_model)
    summary(retina_model, input_size =(1, 3, 1280, 1280))
    model_path = model_path = '/home/orumfels/tfe_my_implementations/object_detection/work_dir/faster_rcnn_resnet50_14_classic_12classes.pth'
    faster_model = load_faster(model_path, class_list)
    summary(faster_model, input_size = (1, 3, 224, 224))
    model_path = '/home/orumfels/tfe_my_implementations/my_resnet/models/triscan/resnet50/data_augment/130.pth'
    resnet_model = load_resnet(model_path)
    ts.summary(resnet_model, (3, 300, 300))
    retinanet_time = []
    faster_time = []
    resnet_time = []
    yolo_time = []
    for i in range(parser.nb_images):
        # random image choice
        choice_int = np.random.randint(0, len(choice_dataset))
        image_name = choice_dataset.get_filename(choice_int)
        sample = choice_dataset[choice_int]
        print("chosen image ",i," =", image_name)
        path = folder_path+ image_name
        image = cv2.imread(path)
        print("RetinaNet inference")
        retinanet_time.append(RetinaNet_inference(path, image_name, class_list, retina_model))
        print("Faster R-CNN inference")
        faster_time.append(Faster_inference(faster_model, class_list, path, image_name))
        print("ResNet inference")
        resnet_time.append(Resnet_inference(resnet_model, path, class_list))
        #print("YOLO inference")
        #yolo_time.append(YOLO_inference(model, image_name, class_list, data))
    
    print("Mean ResNet inference time :", np.mean(np.asarray(resnet_time)))
    print("Mean RetinaNet inference time :", np.mean(np.asarray(retinanet_time)))
    print("Mean Faster R-CNN inference time :", np.mean(np.asarray(faster_time)))
    #print("Mean YOLO inference time :", np.mean(np.asarray(yolo_time)))

if __name__ == '__main__':
    main()