import json
import cv2
# import tfe_my_implementations.my_resnet.utils as utils
from retinanet.dataloader import CocoDataset, CustomDataset, collater, Resizer, Augmenter, Normalizer, transform_data
import numpy as np
import torch
from torchvision import transforms
import skimage.io

def draw_text(dataset, save_folder):
    classes = dataset.get_classes()
    #switch to evaluation mode
    # for all images in the test dataset
    done_class = np.zeros(12)
    for i in range(0, dataset.__len__()):
        filename = dataset.get_filename(i)
        x, y = dataset.__getitem__(i)
        if done_class[y] < 4 :
            done_class[y] = done_class[y] + 1
            # open image and put label on it
            image = cv2.imread(filename)
            text = classes[y]
            #image = cv2.putText(image, text, (100, 500), cv2.FONT_HERSHEY_SIMPLEX ,
            #10, (255, 255, 255), 10, cv2.LINE_AA) # font scale lowered
            name = save_folder + "class"+str(y)+ "_" + str(i) + ".jpg"
            print("Writing image, ", name)
            saved = cv2.imwrite(name, image)
            print("saved ?", saved)
def draw_bbox(dataset, save_folder):
    classes = dataset.get_classes()
    #switch to evaluation mode
    # for all images in the test dataset
    done_class = np.zeros(12)
    for i in range(0, dataset.__len__()):
        sample = dataset.__getitem__(i)
        image = sample['img']
        annot = sample['annot']
        y = int(annot[0][4])
        if done_class[y] < 3 :
            new_sample = transform_data(image, annot)
            new_image = new_sample['img']
            new_annot = new_sample['annot']
            print("annotations :", annot, new_annot)
            done_class[y] = done_class[y] + 1
            text = classes[y]
            
            x_min = int(annot[0, 0])
            x_max = int(annot[0, 2])
            y_min = int(annot[0, 1])
            y_max = int(annot[0, 3])
            image = np.array(image)
            #image = cv2.putText(image, text, (x_min, y_max), cv2.FONT_HERSHEY_SIMPLEX ,1.5, (0, 255, 0), 6, cv2.LINE_AA) # font scale lowered
            image = cv2.rectangle(image, (x_min, y_min), (x_max, y_max), (0, 255, 0), 20)
            x1 = int(new_annot[0, 0])
            x2 = int(new_annot[0, 2])
            y1 = int(new_annot[0, 1])
            y2 = int(new_annot[0, 3])
            new_image = np.array(new_image)
            new_image = cv2.rectangle(new_image, (x1, y1), (x2, y2), (0, 255, 0), 20)
            name = save_folder + "class"+str(y)+ "_" + str(i) + ".jpg"
            new_name = save_folder + "class"+str(y)+ "_" + str(i) + "_box.jpg"
            print("name =", name)
            saved = cv2.imwrite(name, image)
            saved = cv2.imwrite(new_name, new_image)
        
classes = ('Aerosol','Bout./flac. en plastique < 8L', "Bout./flac. en plastique > 8L",
           "Emballage metallique", "Huile de friture", "Carton a boisson", "Papier-carton",
           "Verre incolore", "Verre colore", "FR", "PDP", "DSM")
dataset = CustomDataset("/home/orumfels/datasets/triscandata",
                              classes, train = True)
draw_bbox(dataset, "/home/orumfels/datasets/samples/")
