import argparse
import json
import os
from pathlib import Path
from threading import Thread
import time

import numpy as np
import torch
import yaml
from tqdm import tqdm

from models.experimental import attempt_load
from utils.datasets import create_dataloader
from utils.datasets import LoadStreams, LoadImages
from utils.general import coco80_to_coco91_class, check_dataset, check_file, check_img_size, check_requirements, bbox_iou, non_max_suppression, scale_coords, xyxy2xywh, xywh2xyxy, set_logging, increment_path
from utils.loss import compute_loss
from utils.metrics import ap_per_class, ConfusionMatrix
from utils.plots import plot_images, output_to_target, plot_study_txt
from utils.torch_utils import select_device, time_synchronized
from torchinfo import summary

def compute_overlap(a, b):
    """
    Parameters
    ----------
    a: (N, 4) ndarray of float
    b: (K, 4) ndarray of float
    Returns
    -------
    overlaps: (N, K) ndarray of overlap between boxes and query_boxes
    """
    area = (b[:, 2] - b[:, 0]) * (b[:, 3] - b[:, 1])

    iw = np.minimum(np.expand_dims(a[:, 2], axis=1), b[:, 2]) - np.maximum(np.expand_dims(a[:, 0], 1), b[:, 0])
    ih = np.minimum(np.expand_dims(a[:, 3], axis=1), b[:, 3]) - np.maximum(np.expand_dims(a[:, 1], 1), b[:, 1])

    iw = np.maximum(iw, 0)
    ih = np.maximum(ih, 0)

    ua = np.expand_dims((a[:, 2] - a[:, 0]) * (a[:, 3] - a[:, 1]), axis=1) + area - iw * ih

    ua = np.maximum(ua, np.finfo(float).eps)

    intersection = iw * ih

    return intersection / ua


def _compute_ap(recall, precision):
    """ Compute the average precision, given the recall and precision curves.
    Code originally from https://github.com/rbgirshick/py-faster-rcnn.
    # Arguments
        recall:    The recall curve (list).
        precision: The precision curve (list).
    # Returns
        The average precision as computed in py-faster-rcnn.
    """
    # correct AP calculation
    # first append sentinel values at the end
    mrec = np.concatenate(([0.], recall, [1.]))
    mpre = np.concatenate(([0.], precision, [0.]))

    # compute the precision envelope
    for i in range(mpre.size - 1, 0, -1):
        mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])

    # to calculate area under PR curve, look for points
    # where X axis (recall) changes value
    i = np.where(mrec[1:] != mrec[:-1])[0]

    # and sum (\Delta recall) * prec
    ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
    return ap

def evaluate_map(dataloader, all_detections, all_annotations, top_1_acc):

    average_precisions = {}
    precisions = np.zeros(12)
    recalls = np.zeros(12)
    iou_threshold = 0.5
    iou_mean = 0
    count_iou = 0
    pass_ = 0
    all_detections = np.asarray(all_detections)
    for label in range(12):
        false_positives = np.zeros((0,))
        true_positives  = np.zeros((0,))
        my_tp = 0
        my_fp = 0
        scores          = np.zeros((0,))
        num_annotations = 0.0
        # for all this class, browse all images
        for i in range(len(dataloader)):
            detections           = all_detections[i][label] # image i, label label
            annotations          = all_annotations[i][label]
            if annotations is None :
                shape = 0
            else :
                shape = annotations.shape[0]
            num_annotations     += shape
            detected_annotations = []
            
            if detections is None :
                continue
            for d in detections:
                pass_ += 1
                scores = np.append(scores, d[4])

                if annotations is None : # this class was not in the image yet we detected it
                    false_positives = np.append(false_positives, 1)
                    true_positives  = np.append(true_positives, 0)
                    my_fp += 1
                    continue
                d_0 = np.zeros(5)
                d_0[:] = d[:]
                max_overlap = bbox_iou(torch.Tensor(d_0[:4]), torch.Tensor(annotations), x1y1x2y2=False)
                # max_overlap2 = bbox_iou(torch.Tensor(d_0[:4]), torch.Tensor(annotations), x1y1x2y2=True)
                iou_mean += max_overlap
                count_iou += 1
                assigned_annotation = annotations

                if max_overlap >= iou_threshold and assigned_annotation not in detected_annotations:
                    false_positives = np.append(false_positives, 0)
                    true_positives  = np.append(true_positives, 1)
                    my_tp += 1
                    detected_annotations.append(assigned_annotation)
                else:
                    false_positives = np.append(false_positives, 1)
                    true_positives  = np.append(true_positives, 0)
                    my_fp += 1

        # this class is not in the image
        if num_annotations == 0:
            average_precisions[label] = 0, 0
            continue
        # sort by score
        indices         = np.argsort(-scores)
        false_positives = false_positives[indices]
        true_positives  = true_positives[indices]

        # compute false positives and true positives
        false_positives = np.cumsum(false_positives)
        true_positives  = np.cumsum(true_positives)

        # compute recall and precision
        recall    = true_positives / num_annotations
        precision = true_positives / np.maximum(true_positives + false_positives, np.finfo(np.float64).eps)
        my_precision = my_tp / np.maximum(my_tp + my_fp, np.finfo(np.float64).eps)
        my_recall = my_tp / num_annotations
        precisions[label] = my_precision
        recalls[label] = my_recall

        # compute average precision
        average_precision  = _compute_ap(recall, precision)
        average_precisions[label] = average_precision, num_annotations
    print("pass =", pass_)
    print("mean IOU")
    if count_iou != 0:
        iou_mean /= count_iou
        print(iou_mean)
    else :
        iou_mean = 0
    print('\nmAP:')
    mAP = []
    for label in range(12):
        label_name = str(label)
        print('{}: {} - {} - {}'.format(label_name, average_precisions[label][0], precisions[label], recalls[label]))
        mAP.append(average_precisions[label][0])
        
        """if save_path!=None:
            plt.plot(recall,precision)
            # naming the x axis 
            plt.xlabel('Recall') 
            # naming the y axis 
            plt.ylabel('Precision') 

            # giving a title to my graph 
            plt.title('Precision Recall curve') 

            # function to show the plot
            #plt.savefig(save_path+'/'+label_name+'_precision_recall.jpg')"""

    print("MAP =", np.mean(np.asarray(mAP)))

    return average_precisions, precisions, recalls, iou_mean, pass_, top_1_acc

def test(data,
         weights=None,
         batch_size=1,
         imgsz=640,
         conf_thres=0.25,
         iou_thres=0.6,  # for NMS
         save_json=False,
         single_cls=False,
         augment=False,
         verbose=False,
         model=None,
         dataloader=None,
         save_dir=Path(''),  # for saving images
         save_txt=False,  # for auto-labelling
         save_hybrid=False,  # for hybrid auto-labelling
         save_conf=False,  # save auto-label confidences
         plots=False,
         log_imgs=0):  # number of logged images

    # Initialize/load model and set device
    training = model is not None
    if training:  # called by train.py
        device = next(model.parameters()).device  # get model device

    else:  # called directly
        set_logging()
        device = select_device(opt.device, batch_size=batch_size)

        # Directories
        save_dir = Path(increment_path(Path(opt.project) / opt.name, exist_ok=opt.exist_ok))  # increment run
        (save_dir / 'labels' if save_txt else save_dir).mkdir(parents=True, exist_ok=True)  # make dir

        # Load model
        model = attempt_load(weights, map_location=device)  # load FP32 model
        imgsz = check_img_size(imgsz, s=model.stride.max())  # check img_size

        # Multi-GPU disabled, incompatible with .half() https://github.com/ultralytics/yolov5/issues/99
        # if device.type != 'cpu' and torch.cuda.device_count() > 1:
        #     model = nn.DataParallel(model)

    # Half
    half = device.type != 'cpu'  # half precision only supported on CUDA
    if half:
        model.half()
    summary(model, input_size =(1, 3, 1280, 1280))
    # Configure
    model.eval()
    is_coco = data.endswith('coco.yaml')  # is COCO dataset
    with open(data) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)  # model dict
    check_dataset(data)  # check
    nc = 1 if single_cls else int(data['nc'])  # number of classes
    iouv = torch.linspace(0.5, 0.95, 10).to(device)  # iou vector for mAP@0.5:0.95
    niou = iouv.numel()

    # Logging
    log_imgs, wandb = min(log_imgs, 100), None  # ceil
    try:
        import wandb  # Weights & Biases
    except ImportError:
        log_imgs = 0

    # Dataloader
    if not training:
        img = torch.zeros((1, 3, imgsz, imgsz), device=device)  # init img
        _ = model(img.half() if half else img) if device.type != 'cpu' else None  # run once
        path = data['test'] if opt.task == 'test' else data['val']  # path to val/test images
        dataloader = create_dataloader(path, imgsz, batch_size, model.stride.max(), opt, pad=0.5, rect=True)[0]

    seen = 0
    confusion_matrix = ConfusionMatrix(nc=nc)
    names = {k: v for k, v in enumerate(model.names if hasattr(model, 'names') else model.module.names)}
    coco91class = coco80_to_coco91_class()
    s = ('%20s' + '%12s' * 6) % ('Class', 'Images', 'Targets', 'P', 'R', 'mAP@.5', 'mAP@.5:.95')
    p, r, f1, mp, mr, map50, map, t0, t1 = 0., 0., 0., 0., 0., 0., 0., 0., 0.
    loss = torch.zeros(3, device=device)
    jdict, stats, ap, ap_class, wandb_images = [], [], [], [], []
    
    all_detections = [[None for i in range(12)] for j in range(len(dataloader))]
    all_annotations = [[None for i in range(12)] for j in range(len(dataloader))]
    max_detections = 100
    
    img_count = 0
    dic_pred = {}
    dic_real = {}
    dic_best_pred = {}
    found_label = 0
    top_1_acc = 0
    tmp = -1
    some_count = 0
    times = []
    for batch_i, (img, targets, paths, shapes) in enumerate(tqdm(dataloader, desc=s)):
        img = img.to(device, non_blocking=True)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        targets = targets.to(device)
        nb, _, height, width = img.shape  # batch size, channels, height, width
        
        with torch.no_grad():
            # Run model
            t = time_synchronized()
            start = time.time()
            inf_out, train_out = model(img, augment=augment)  # inference and training outputs
            end = time.time()
            t0 += time_synchronized() - t
            times.append(end -start)

            # Compute loss
            if training:
                loss += compute_loss([x.float() for x in train_out], targets, model)[1][:3]  # box, obj, cls
            # HERE WE GOT ALL DETECTIONS

            targets[:, 2:] *= torch.Tensor([width, height, width, height]).to(device)  # to pixels
            lb = [targets[targets[:, 0] == i, 1:] for i in range(nb)] if save_hybrid else []  # for autolabelling
            t = time_synchronized()
            output = non_max_suppression(inf_out, conf_thres=conf_thres, iou_thres=iou_thres, labels=lb)
            #output = non_max_suppression(inf_out, conf_thres=conf_thres, iou_thres=iou_thres, classes=None, agnostic=False)
            t1 += time_synchronized() - t
            # Here we got all outputs
            # Get detections
            for si, pred in enumerate(output):
                pred = pred.cpu()
                scores = np.zeros(shape = (len(pred)))
                boxes = np.zeros(shape = (len(pred), 4))
                labels = np.zeros(shape = (len(pred)))
                # for each image get boxes and scores
                for index, (x1,x2,x3,x4,score,label) in enumerate(pred):
                    scores[index] = score
                    #new_box = scale_coords(img[si].shape[1:], pred[:4], shapes[si][0], shapes[si][1])
                    boxes[index, :] = [x1 + (x3/2), x2 + (x4/2), x3, x4][:]#new_box[0, :4]## [x1, x2, x3, x4][:]#
                        
                    labels[index] = label
                    label = label.numpy()
                    label = int(label)
                    if label in dic_pred:
                        dic_pred[label] = dic_pred[label] + 1
                    else :
                        dic_pred[label] = 1
                    if score > 0.05 :
                        if label in dic_best_pred:
                            dic_best_pred[label] = dic_best_pred[label] + 1
                        else :
                            dic_best_pred[label] = 1
                indices = np.where(scores > 0.05)[0]
                if indices.shape[0] > 0:
                    # select those scores
                    scores = scores[indices]

                    # find the order with which to sort the scores
                    scores_sort = np.argsort(-scores)[:max_detections]

                    # select detections
                    image_boxes      = boxes[indices[scores_sort], :]
                    image_scores     = scores[indices[scores_sort]]
                    image_labels     = labels[indices[scores_sort]]
                    tmp = image_labels[0]
                    image_detections = np.concatenate([image_boxes, np.expand_dims(image_scores, axis=1), np.expand_dims(image_labels, axis=1)], axis=1)

                    # copy detections to all_detections
                    for label in range(12):
                        all_detections[batch_i][label] = np.asarray(image_detections[image_detections[:, -1] == label, :-1])
                else:
                    # copy detections to all_detections
                    for label in range(12):
                        all_detections[img_count][label] = np.zeros((0, 5))
            
            # get annotations
            #  targets[0] contains : image id, class, and bounding box
            t_id, label, x1, x2, x3, x4 = targets[0]
            label = label.cpu()
            label = label.numpy()
            some_count += 1
            me_to_them = [0, 1, 2, 5, 11, 3, 9, 4, 10, 6, 8, 7]
            them_to_me = [0, 1, 2, 5, 7, 3, 9, 11, 10, 6, 8, 4]
            label = me_to_them[int(label)]
            if int(tmp) ==  int(label):
                top_1_acc += 1
            str_label = int(label)
            if str_label in dic_real:
                dic_real[str_label] = dic_real[str_label] + 1
            else :
                dic_real[str_label] = 1
            annot = np.zeros(shape =(1, 4))
            annot[0, :] = [x1, x2, x3, x4][:]
            for i in range(12):
                if int(label) == i:
                    all_annotations[batch_i][i] = annot.copy()
                    found_label += 1
                else :
                    all_annotations[batch_i][i] = None
    print("\n real", dic_real, "\n predicted =", dic_pred, "\n above threshold predictions =", dic_best_pred, img_count)
    print("top 1 acc = ", top_1_acc/len(dataloader))
    print("Mean inference time =", np.mean(np.asarray(times)))
    # print("\n \n \n \n \n ALL ANNOTATIONS \n \n \n \n \n", all_annotations)
    found = 0
    for i in range(len(dataloader)):
        for j in range(12):
            if all_annotations[i][j] is not None :
                found += 1
    print("found label = ", found_label)
    return evaluate_map(dataloader, all_detections, all_annotations, top_1_acc/len(dataloader))

        


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='test.py')
    parser.add_argument('--weights', nargs='+', type=str, default='yolov5s.pt', help='model.pt path(s)')
    parser.add_argument('--data', type=str, default='data/coco128.yaml', help='*.data path')
    parser.add_argument('--batch-size', type=int, default=32, help='size of each image batch')
    parser.add_argument('--img-size', type=int, default=640, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.05, help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.6, help='IOU threshold for NMS')
    parser.add_argument('--task', default='val', help="'val', 'test', 'study'")
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--single-cls', action='store_true', help='treat as single-class dataset')
    parser.add_argument('--augment', action='store_true', help='augmented inference')
    parser.add_argument('--verbose', action='store_true', help='report mAP by class')
    parser.add_argument('--save-txt', action='store_true', help='save results to *.txt')
    parser.add_argument('--save-hybrid', action='store_true', help='save label+prediction hybrid results to *.txt')
    parser.add_argument('--save-conf', action='store_true', help='save confidences in --save-txt labels')
    parser.add_argument('--save-json', action='store_true', help='save a cocoapi-compatible JSON results file')
    parser.add_argument('--project', default='runs/test', help='save to project/name')
    parser.add_argument('--name', default='exp', help='save to project/name')
    parser.add_argument('--exist-ok', action='store_true', help='existing project/name ok, do not increment')
    opt = parser.parse_args()
    opt.save_json |= opt.data.endswith('coco.yaml')
    opt.data = check_file(opt.data)  # check file
    print(opt)
    check_requirements()

    if opt.task in ['val', 'test']:  # run normally
        test(opt.data,
             opt.weights,
             opt.batch_size,
             opt.img_size,
             opt.conf_thres,
             opt.iou_thres,
             opt.save_json,
             opt.single_cls,
             opt.augment,
             opt.verbose,
             save_txt=opt.save_txt | opt.save_hybrid,
             save_hybrid=opt.save_hybrid,
             save_conf=opt.save_conf,
             )

    elif opt.task == 'study':  # run over a range of settings and save/plot
        for weights in ['yolov5s.pt', 'yolov5m.pt', 'yolov5l.pt', 'yolov5x.pt']:
            f = 'study_%s_%s.txt' % (Path(opt.data).stem, Path(weights).stem)  # filename to save to
            x = list(range(320, 800, 64))  # x axis
            y = []  # y axis
            for i in x:  # img-size
                print('\nRunning %s point %s...' % (f, i))
                r, _, t = test(opt.data, weights, opt.batch_size, i, opt.conf_thres, opt.iou_thres, opt.save_json,
                               plots=False)
                y.append(r + t)  # results and times
            np.savetxt(f, y, fmt='%10.4g')  # save
        os.system('zip -r study.zip study_*.txt')
        plot_study_txt(f, x)  # plot

